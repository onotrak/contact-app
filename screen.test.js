import React from 'react';
import { render } from '@testing-library/react-native';
import Routing from './src/navigations/Routing';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

jest.mock('@react-navigation/native', () => ({
  NavigationContainer: ({ children }) => <>{children}</>,
}));

jest.mock('@react-navigation/native-stack', () => ({
  createNativeStackNavigator: jest.fn(),
}));

describe('Routing', () => {
  beforeEach(() => {
    createNativeStackNavigator.mockClear();
  });

  it('renders NavigationContainer', () => {
    render(<Routing />);
    expect(NavigationContainer).toHaveBeenCalled();
  });

  it('renders Stack.Navigator with correct screen names and components', () => {
    render(<Routing />);
    expect(createNativeStackNavigator).toHaveBeenCalledWith();
    expect(createNativeStackNavigator).toHaveBeenCalledWith({
      Screen: expect.any(Function),
    });

    const stackOptions = createNativeStackNavigator.mock.calls[0][0];
    const screenNames = ['Splash', 'Home', 'Detail', 'AddData'];

    screenNames.forEach((screenName) => {
      expect(stackOptions.Screen).toHaveBeenCalledWith({
        name: screenName,
        component: expect.any(Function),
      });
    });
  });

  it('does not render header in Stack.Navigator', () => {
    render(<Routing />);
    const stackOptions = createNativeStackNavigator.mock.calls[0][0];
    expect(stackOptions.headerShown).toBe(false);
  });
});
