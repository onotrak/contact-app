import React, {useEffect, useState} from 'react';
import API from '../utils/service/apiProvider';
import { useDispatch } from 'react-redux';
import {version} from '../../package.json';
import SplashComponent from '../component/sections/SplashComponent';

const Splash = ({navigation, route}) => {
	const dispatch = useDispatch();
	useEffect(() => {
		onGetData();
	}, [])

	const onGetData = async () => {
		const res = await API.getDataContact();
		if (res?.data?.length) {
			const data = res?.data;
			dispatch({type: "ADD_DATA", data});
		} else {
		}
		navigation.replace('Home');
	}

	return(
		<SplashComponent version={version} />
	)
}

export default Splash;