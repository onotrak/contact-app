import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import DetailComponent from '../component/sections/DetailComponent';
import Loader from '../component/globals/Loader';
import apiProvider from '../utils/service/apiProvider';
import { Helper } from '../utils';
import { Alert } from 'react-native';

const Detail = ({navigation, route}) => {
   const [loading, setLoading] = useState(false);
   const [data, setData] = useState(null);
   const dataContact = route?.params?.dataContact?.length ? route.params.dataContact : [];

   useEffect(() => {
      getDetail();
   }, [])

   const getDetail = async (val) => {
		setLoading(true);
      const id = val ?? route?.params?.data?.id ?? '';
		const res = await apiProvider.getDataContact(`/${id}`);
		if (res?.data) {
			setData(res.data);
		} else {
         Helper.showToastMessage(res?.message ?? 'Error')
		}
		setLoading(false);
	}

   const onDeleteData = async () => {
		setLoading(true);
      const id = route?.params?.data?.id ?? '';
		const res = await apiProvider.deleteContact(`/${id}`);
		if (res?.status === 202) {
         await route?.params?.onGoback ? route.params.onGoback() : null;
         Helper.showToastMessage(res?.statusText ?? 'Data Deleted')
         navigation.goBack();
		} else {
         Helper.showToastMessage(res?.message ?? 'Error')
		}
      setLoading(false)
      
	}

   const validationDelete = () => {
      Alert.alert(
         "Confirmation",
         "You sure want to delete this contact?",
         [
            {
               text: "Cancel",
               onPress: () => console.log("Cancel Pressed"),
               style: "cancel"
            },
            { text: "DELETE", onPress: () => onDeleteData() }
         ]
      );
   }

   return (
      <DetailComponent dataDetail={data} onDeleteData={validationDelete} getDetail={getDetail} data={dataContact} loading={loading} />
   )
}

export default Detail