import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import apiProvider from '../utils/service/apiProvider';
import AddDataComponent from '../component/sections/AddDataComponent';
import { Helper } from '../utils';

const AddData = ({navigation, route}) => {
   const [loading, setLoading] = useState(false);
   const [errorStatus, setErrorStatus] = useState(false);
   const [modalImage, setModalImage] = useState(false);
   const [state, setState] = useState({
      firstName: '',
      lastName: '',
      age: '',
      photo: '',
   });

   const data = route?.params?.data;

   useEffect(() => {
      if(data?.id){
         const params = {
            ...data,
            age: data?.age ? data?.age?.toString() : ''
         };
         setState(params);
      }
   }, [])
   
   const checkInput = () => {
      if (state.firstName === '' || state.lastName === '' || state.age === '' || state.photo === '') {
         return true;
      }
      return false;
   };

   const onSelectImage = (val) => {
      setModalImage(false);
      setState({...state, photo: val});
   };

   const addData = async () => {
      if(checkInput()){
         return setErrorStatus(true);
      }
		setLoading(true);
      const params = {
         ...state,
         age: parseInt(state.age),
      };
		const res = await apiProvider.addContact(params);
		if (res?.status === 201) {
         await route?.params?.onGoback ? route.params.onGoback() : null;
         Helper.showToastMessage(res?.message ?? 'Success Add New Contact')
         navigation.goBack();
		} else {
         Helper.showToastMessage(res?.message ?? 'Error')
		}
		setLoading(false);
	}

   const updateData = async () => {
      if(checkInput()){
         return setErrorStatus(true);
      }
		setLoading(true);
      const id = data?.id ? `/${data?.id}` : '';
      const params = {
         ...state,
         age: parseInt(state.age)
      };
		const res = await apiProvider.updateContact(params, id);
		if (res?.status === 201) {
         await route?.params?.onGoback ? route.params.onGoback() : null;
         Helper.showToastMessage(res?.message ?? 'Success Update Contact')
         navigation.goBack();
		} else {
         Helper.showToastMessage(res?.message ?? 'Error')
		}
		setLoading(false);
	}
   
   return (
      <AddDataComponent 
         loading={loading} 
         onAddData={data ? updateData : addData} 
         state={state}
         setState={(key, value) => setState({...state, [key]: value})}
         isEdit={data}
         onSelectImage={onSelectImage}
         setModalImage={setModalImage}
         modalImage={modalImage}
         checkInput={checkInput}
         errorStatus={errorStatus}
         setErrorStatus={setErrorStatus}
      />
   )
}

export default AddData