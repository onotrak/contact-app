import React, {useEffect, useState} from 'react';
import API from '../utils/service/apiProvider';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import HomeComponent from '../component/sections/HomeComponent';

const Home = ({navigation, route}) => {
	const dispatch = useDispatch();
	const isFocused = useIsFocused();
	const [showModalAdd, setShowModalAdd] = useState(false);
	const [refreshing, setRefreshing] = useState(false);
	const [keyword, setKeyword] = useState('');
	const [showHistory, setShowHistory] = useState('');
	const [data, setData] = useState([]);
	const [oriData, setOriData] = useState([]);
	const [checked, setChecked] = useState(null);

	useEffect(() => {
		onGetData();
	}, [isFocused])

	const onRefresh = () => {
		setKeyword('');
		onGetData();
	}

	const onGetData = async () => {
		setRefreshing(true);
		const res = await API.getDataContact();
		if (res?.data?.length) {
			const data = res?.data;
			setData(data);
			setOriData(data);
		} else {
		}
		setRefreshing(false);
	}

	const onSaveHistory = () => {
		const data = keyword;
		if(keyword?.length >=3){
			dispatch({type: "ADD_KEYWORD", data});
		}
	}

	const onDeleteItem = (val) => {
		const data = val;
		dispatch({type: "DELETE_KEYWORD", data});
	}

	const onSearch = (val) => {
		const search = val ?? keyword;
		setShowHistory(false);
		onSaveHistory();
		setData(filterSearch(search));
	}

	const filterSearch = (val='')=>{
		return oriData.filter((x) => {
			return validationSearch(x?.firstName, val) !== -1
				||  validationSearch(x?.lastName, val) !== -1
		})
	}

	const validationSearch = (val, key='') => {
		const text = val ?? '';
		const data = text?.toLowerCase().search(key?.toLowerCase());
		return data;
	}

	const onSort = (val) => {
		setShowModalAdd(false);
		switch (val) {
			case 'ASC':
				setData(sortASC(data))
				break;
			case 'DESC':
				setData(sortDESC(data))
				break;
		
			default:
				break;
		}
	}

	const sortASC = (array) => {
		return array.sort((a, b) => {
			const firstNameA = a.firstName.toUpperCase();
			const firstNameB = b.firstName.toUpperCase();

			if (firstNameA < firstNameB) {
				return -1;
			}
			if (firstNameA > firstNameB) {
				return 1;
			}
			return 0;
		});
	};

	const sortDESC = (array) => {
		return array.sort((a, b) => {
			const firstNameA = a.firstName.toUpperCase();
			const firstNameB = b.firstName.toUpperCase();

			if (firstNameA > firstNameB) {
				return -1;
			}
			if (firstNameA < firstNameB) {
				return 1;
			}
			return 0;
		});
	};

	return(
		<HomeComponent 
			data={data} 
			setShowModalAdd={setShowModalAdd}
			showModalAdd={showModalAdd}
			onGetData={onGetData}
			refreshing={refreshing}
			onRefresh={onRefresh}

			onPressMenu={()=> {}}
			keyword={keyword}
			setKeyword={setKeyword}
			showHistory={showHistory}
			setShowHistory={setShowHistory}
			saveHistory={onSaveHistory}
			onSearch={onSearch}
			onDeleteItem={onDeleteItem}
			onSort={onSort}
			checked={checked}
			setChecked={setChecked}
		/>
	)
}

export default Home;