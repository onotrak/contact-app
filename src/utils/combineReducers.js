import {combineReducers} from 'redux';
import dataReducer from './reducers/dataReducer';
import historyReducer from './reducers/historyReducer';
import formReducer from './reducers/formReducer';

const rootReducer = combineReducers({
	data: dataReducer,
	history: historyReducer,
	form: formReducer,
})

export default rootReducer;