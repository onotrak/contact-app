import API from './axiosConfig';

export default {
	getDataContact: async (id='') => {
		return API(`contact${id}`, {
			method: 'GET',
			head: {
				'Content-Type': 'application/json',
			},
		})
			.then((response) => {return response})
			.catch((err) => {return err});
	},
	addContact: async (params) => {
		return API(`contact`, {
			method: 'POST',
			head: {
				Accept: "application/json",
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(params),
		})
			.then((response) => {return response})
			.catch((err) => {return err});
	},
	deleteContact: async (id='') => {
		return API(`contact${id}`, {
			method: 'DELETE',
			head: {
				Accept: "application/json",
				'Content-Type': 'application/json',
			},
		})
			.then((response) => {return response})
			.catch((err) => {return err});
	},
	updateContact: async (params, id='') => {
		return API(`contact${id}`, {
			method: 'PUT',
			head: {
				Accept: "application/json",
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(params),
		})
			.then((response) => {return response})
			.catch((err) => {return err});
	},
};
