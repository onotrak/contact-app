export const BackWhite = require('../assets/icons/back_white_ic.png');
export const ArrowBack = require('../assets/icons/arrow_back_ic.png');
export const LeftArrow = require('../assets/icons/left_arrow_ic.png');
export const Search = require('../assets/icons/search_ic.png');
export const Home = require('../assets/icons/home_ic.png');
export const Setting = require('../assets/icons/setting_ic.png');
export const ContactLogo = require('../assets/icons/contact_logo_ic.png');
export const Image = require('../assets/icons/image_ic.png');