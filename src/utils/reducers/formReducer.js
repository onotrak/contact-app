const initialState = {
    imageForm: [
        'https://t4.ftcdn.net/jpg/04/48/00/73/360_F_448007337_S7BFhvSy3uzFbe5D3k0MgPEHeCgT08gM.jpg',
        'https://blog.castle.io/content/images/2021/03/blog-thumb-1.png',
        'https://www.techugo.com/blog/wp-content/uploads/2018/09/How-Is-The-React-Native-Changing-The-App-Technology.jpg',
        'https://e7.pngegg.com/pngimages/173/1006/png-clipart-react-web-development-iphone-8-firebase-others-miscellaneous-gadget.png',
        'https://www.rumahcoding.co.id/wp-content/uploads/2020/11/React-Native-1024x1024.png',
        'https://cdn3d.iconscout.com/3d/free/thumb/free-react-native-5562339-4642743.png',
        'https://blog.logrocket.com/wp-content/uploads/2021/08/react-native-svg-tutorial-examples.png',
        'https://blog.logrocket.com/wp-content/uploads/2020/07/react-native-geolocation.png',
        'https://resources.reed.co.uk/courses/coursemedia/343832/504b07d4-871d-412b-bf80-5fc1eb9c75dc_cover.jpg',
        'https://blog.logrocket.com/wp-content/uploads/2022/03/react-tic-tac-toe-hooks.png',
        'https://w7.pngwing.com/pngs/18/497/png-transparent-black-and-blue-atom-icon-screenshot-react-javascript-responsive-web-design-github-angularjs-github-logo-electric-blue-signage.png',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQof8DNWmxkNJX59_VKPdsAF_805yD2vsfKuMr_XedMSNyWgYyZG-bjw05w1zAXGrnPgew&usqp=CAU',
        'https://pbs.twimg.com/profile_images/1052932890332463106/qts9ublT_400x400.jpg',
    ],
};

const formReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_FORM':
            return {
				...state,
				imageForm: action.data,
			};
        case 'DELETE_FORM':
            return {
                ...state,
                imageForm: [],
            };
        case 'RESET_FORM':
            return {
                ...state,
                imageForm: [],
            };
        default:
            return state;
    }
}
export default formReducer;