const initialState = {
	history: []
};

const historyReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'ADD_KEYWORD':
			var newData = [...state.history];
			var findIndex = state.history.findIndex((val) => {return val === action.data});
			if(findIndex !== -1){
				newData[findIndex] = action.data;
				return {
					...state,
					history: newData.sort(),
				};
			}else{
				newData.push(action.data)
				return {
					...state,
					history: newData.sort(),
				};
			}
		case 'DELETE_KEYWORD':
			var newData = [...state.history];
			var findIndex = state.history.findIndex((val) => {return val === action.data});
			newData.splice(findIndex, 1);
			return {
				...state,
				history: newData.sort(),
			};
		case 'RESET_KEYWORD':
			return {
				...state,
				history: [],
			};
		default:
			return state;
	}
}
export default historyReducer;