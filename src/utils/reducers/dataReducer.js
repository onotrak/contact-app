const initialState = {
    data: [],
};

const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_DATA':
            return {
				...state,
				data: action.data,
			};
        case 'DELETE_DATA':
            return {
                ...state,
                data: [],
            };
        case 'RESET':
            return {
                ...state,
                data: [],
            };
        default:
            return state;
    }
}
export default dataReducer;