import { Dimensions, Image, ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import { InputText } from '../globals/InputText'
import { Colors, Mixins } from '../../styles'
import { Touchable } from '../globals/Touchable'
import { TextBold, TextRegular } from '../globals/Text'
import { Header } from '../globals/Header'
import Loader from '../globals/Loader'
import { IconAsset } from '../../utils'
import ModalImage from '../modals/ModalImage'

const WIDTH = Dimensions.get('window').width;

const AddDataComponent = ({
   loading,
   onAddData,
   state,
   setState,
   isEdit,
   errorStatus,
   onSelectImage,
   modalImage,
   setModalImage,
   setErrorStatus,
   checkInput,
}) => {
  return (
      <View style={styles.container}>
         <Loader show={loading}/>
         <ModalImage show={modalImage} onSelectImage={onSelectImage} onClose={()=> setModalImage(false)} onSelect={onSelectImage} />
         <Header style={{backgroundColor: Colors.WHITE}} iconBackColor={Colors.GREY_DARK} title={isEdit ? 'Edit Contact':'Add Contact'} />
         <ScrollView>
            <View style={styles.content}>
               <InputText
                  value={state.firstName}
                  onChangeText={(text) => setState('firstName', text)}
                  style={styles.inputText}
                  placeholder="First Name"
                  message={errorStatus && !state.firstName? 'First Name is Required':null}
                  messageColor={Colors.RED}
               />
               <InputText
                  value={state.lastName}
                  onChangeText={(text) => setState('lastName', text)}
                  style={styles.inputText}
                  placeholder="Last Name"
                  message={errorStatus && !state.lastName? 'Last Name is Required':null}
                  messageColor={Colors.RED}
               />
               <InputText
                  value={state.age}
                  onChangeText={(text) => setState('age', text)}
                  style={styles.inputText}
                  keyboardType='number-pad'
                  placeholder="Age"
                  message={errorStatus && !state.age? 'Age is Required':null}
                  messageColor={Colors.RED}
               />
               <InputText
                  value={state.photo}
                  onChangeText={(text) => setState('photo', text)}
                  style={styles.inputText}
                  placeholder="Select or Paste URL Photo"
                  icon={IconAsset.Image}
                  iconColor={Colors.PRIMARY_BLUE}
                  onPressRightBtn={()=> setModalImage(true)}
                  message={errorStatus && !state.photo? 'URL Photo is Required':null}
                  messageColor={Colors.RED}
               />
               {
                  state?.photo && state.photo.includes('http') ? 
                  <Image source={{uri: state.photo}} style={styles.imgPhoto} />
                  :null
               }
            </View>
         </ScrollView>
         <View style={styles.viewButtonAdd}>
            <Touchable onPress={onAddData} style={styles.buttonAdd}>
               <TextBold text={isEdit ? 'Update':'Save'} size={16} color={Colors.WHITE} />
            </Touchable>
         </View>
      </View>
   )
}

export default AddDataComponent

const styles = StyleSheet.create({
   container: {
      flex: 1,
		backgroundColor: Colors.NEUTRAL,
   },
   content: {
      padding: 20,
      alignItems: 'center',
      height: '90%',
      marginBottom: 60,
   },
   inputText: {
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      borderRadius: Mixins.scaleSize(10),
      paddingHorizontal: Mixins.scaleSize(16),
      paddingVertical: Mixins.scaleSize(5),
      width: '100%',
      alignItems: 'center',
   },
   buttonAdd: {
      backgroundColor: Colors.PRIMARY_BLUE,
      paddingVertical: 10,
      width: WIDTH-40,
      borderRadius: 8,
      alignItems: 'center',
   },
   viewButtonAdd: {
      position: 'absolute',
      bottom: 15,
      width: WIDTH,
      alignItems: 'center',
   },
   imgPhoto: {
      width: 200,
      height: 200,
      borderWidth: 1,
      borderColor: Colors.PRIMARY,
      borderRadius: Mixins.scaleSize(10),
   },
})