import { ActivityIndicator, Dimensions, FlatList, Image, RefreshControl, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { TextBold, TextMedium, TextRegular } from '../globals/Text';
import { Header } from '../globals/Header';
import { Helper, IconAsset, ImageAsset } from '../../utils';
import { Colors, Mixins, Shadow } from '../../styles';
import { Touchable } from '../globals/Touchable';
import { useNavigation } from '@react-navigation/native';
import ModalAddData from '../modals/ModalAddData';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useSelector } from 'react-redux';

const HomeComponent = ({
	data, 
	showModalAdd,
	setShowModalAdd,
	onGetData,
	refreshing,
	onRefresh,
	onPressMenu,
	keyword,
	setKeyword,
	showHistory,
	setShowHistory,
	onSearch,
	onDeleteItem,
	onSort,
	checked,
	setChecked,
}) => {
	const navigation = useNavigation();
	const { history } = useSelector((state) => state.history);

   return (
		<View style={styles.container}>
			<ModalAddData 
				backgroundClose 
				show={showModalAdd} 
				onClose={()=> setShowModalAdd(false)} 
				onSort={onSort} 
				checked={checked}
				setChecked={setChecked}
			/>
			<View style={styles.viewHeader}>
				<TextBold text={'Contact App'} size={20} color={Colors.TEXT_3} />
				<Touchable onPress={onRefresh} style={styles.iconTitleRight}>
					{
						refreshing ? 
						<View style={styles.loaderHeader}>
							<ActivityIndicator size={'small'} color={Colors.PRIMARY_BLUE}/>
						</View>
						:
						<Ionicons name='sync-circle-outline' size={30} color={Colors.PRIMARY_BLUE} />
					}
				</Touchable>
			</View>
			<View style={styles.content}>
				<SearchComponent 
					onSearch={onSearch}
					keyword={keyword}
					setKeyword={setKeyword}
					onPressMenu={()=> setShowModalAdd(true)}
					show={showHistory}
					data={history}
					setShow={setShowHistory}
					onDeleteItem={onDeleteItem}
				/>
				<FlatList
					data={data}
					renderItem={({item}) => 
						<Item 
							name={`${item.firstName} ${item.lastName}`} 
							photo={item.photo} 
							onPressDetail={()=> {
								navigation.navigate('Detail', {
									data: item,
									dataContact: data,
									onGoBack: () => {
										onGetData();
									}
								})
							}} 
						/>
					}
					refreshControl={
						<RefreshControl 
							colors={[Colors.PRIMARY_BLUE]} 
							refreshing={refreshing} 
							onRefresh={onRefresh} 
						/>
					} 
					ListEmptyComponent={<ListEmptyComponent refreshing={refreshing}/>}
					keyExtractor={item => item.id}
				/>
			</View>
			<View style={styles.viewFloatingButton}>
				<Touchable 
					onPress={()=> {
						navigation.navigate('AddData', {
							onGoBack: () => {
								onGetData();
							}
						});
					}}
					style={styles.floatingButton}>
					<Ionicons name='add' size={30} color={Colors.PRIMARY_BLUE} />
				</Touchable>
			</View>
		</View>
   )
}

const SearchComponent = ({
	onSearch,
	keyword,
	setKeyword,
	onPressMenu,
	show,
	data=[],
	setShow,
	onDeleteItem,
}) => {
	return(
		<View style={styles.contentSearch}>
			<View style={styles.firstContent}>
				<View 
					style={[
						styles.viewForm, 
						show && data?.length ? {
							borderBottomLeftRadius: 0,
							borderBottomRightRadius: 0,
							borderBottomWidth: 0,
						}:{}
					]}
				>
					<TextInput
						placeholder="Search…"
						placeholderTextColor={Colors.GREY_3}
						keyboardType='default'
						value={keyword}
						onChangeText={(text) => setKeyword(text)}
						onSubmitEditing={()=> {
							onSearch();
							setShow(false);
						}}
						style={styles.form}
						onFocus={()=> setShow(true)}
					/>
					<Touchable onPress={onSearch}>
						<Ionicons name='search' size={20} color={Colors.PRIMARY_BLUE} />
					</Touchable>
				</View>
				<Touchable onPress={onPressMenu} style={styles.buttonFilter}>
					<Ionicons name='menu' size={25} color={Colors.PRIMARY_BLUE} />
				</Touchable>
			</View>
			{
				show && data.length ?
				<View>
					<TouchableOpacity activeOpacity={1} onPress={()=> setShow(false)} style={styles.backgroundClose}/>
					<View style={styles.dropDownContainer}>
						<ScrollView nestedScrollEnabled={true}>
						{
							data.map((item) => {
								return (
									<Touchable 
										onPress={() => {
											setKeyword(item);
											setShow(false);
											onSearch(item);
										}} 
										style={styles.dropDownItem}
									>
										<TextRegular text={`${item}`} style={{width: '90%'}} color={Colors.TEXT_2}/>
										<Touchable onPress={()=> onDeleteItem(item)} style={{width: 30}}>
											<Ionicons name='close' size={20} color={Helper.opacityColor(Colors.RED_1, 6)} />
										</Touchable>
									</Touchable>
								);
							})
						}
						</ScrollView>
					</View>
				</View>
				:null
			}
		</View>
	)
}

const Item = ({name, photo, onPressDetail}) => (
	<Touchable onPress={onPressDetail} style={styles.item}>
		{/* <Image source={photo && photo !== 'N/A' ? {uri: photo} : ImageAsset.NotAvailable} style={styles.imgPhoto} /> */}
		{
			photo && photo.includes('http') ? 
			<Image source={{uri: photo}} style={styles.imgPhoto} />
			:
			<View style={styles.iconCard}>
				<TextBold text={Helper.iconItem(name)} size={20} color={Colors.PRIMARY_BLUE} />
			</View>
		}
		<TextRegular text={name} style={styles.textName} color={Colors.TEXT} />
	</Touchable>
);

const ListEmptyComponent = ({refreshing}) => (
	<View>
		{
			refreshing ?
			<View style={styles.contentEmptyList}>
				<View style={[styles.emtyList, {marginBottom: 20}]}>
					<AntDesign name='clouddownloado' size={70} color={Colors.PRIMARY_BLUE} />
				</View>
				<TextMedium text={'Loading . . .'} color={Colors.TEXT_3} size={18} style={styles.titleEmpty} />
			</View>
			:
			<View style={styles.contentEmptyList}>
				<TextMedium text={'Your contact is empty'} color={Colors.TEXT_3} size={18} style={styles.titleEmpty} />
				<TextRegular text={'Please add or refresh list.'} color={Colors.TEXT_2} />
				<View style={styles.emtyList}>
					<AntDesign name='contacts' size={70} color={Colors.PRIMARY_BLUE} />
				</View>
			</View>
		}
	</View>
);

export default HomeComponent

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.WHITE,
	},
	content: {
		backgroundColor: Colors.NEUTRAL,
		height: Dimensions.get('window').height-80,
	},
	viewHeader: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 20,
		alignItems: 'center',
		paddingVertical: 10,
		borderBottomColor: Colors.GREY_2,
		borderBottomWidth: 1,
	},
	item: {
		marginBottom: 10,
		marginHorizontal: 30,
		paddingVertical: 10,
		flexDirection: 'row',
		borderBottomColor: Colors.GREY_BORDER,
		borderBottomWidth: 1,
		alignItems: 'center'
	},
	textName: {
		fontSize: 15,
		marginLeft: 20,
	},
	imgPhoto: {
		width: 50,
		height: 50,
		borderRadius: 100,
		resizeMode: 'cover'
	},
	iconTitleRight: {
		paddingHorizontal: 20,
	},
	viewFloatingButton: {
		position: 'absolute',
		bottom: 30,
		right: 20,
		elevation: 3,
		borderRadius: 30,
		borderColor: Colors.BLUE_NAVY,
		backgroundColor: Colors.NEUTRAL_1,
	},
	floatingButton: {
		padding: 5,
		paddingHorizontal: 6,
	},
	iconCard: {
		width: 48.5,
		height: 48.5,
		borderRadius: 48.5,
		alignItems: 'center',
		justifyContent: 'center',
		borderColor: Colors.PRIMARY_BLUE,
		borderWidth: 1.5,
	},
	emtyList: {
		marginTop: 30,
		padding: 30,
		paddingRight: 35,
		borderRadius: 200,
		backgroundColor: Colors.NEUTRAL_1
	},
	contentEmptyList: {
		padding: 20,
		alignItems: 'center',
		alignContent: 'center',
	},
	titleEmpty: {
		marginBottom: 5,
	},
	contentSearch: {
		paddingBottom: 10,
	},
	firstContent: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingTop: 10,
		paddingHorizontal: 15,
	},
	viewForm: {
		flexDirection: 'row',
		width: 300,
		height: 43,
		backgroundColor: Colors.WHITE,
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: 14,
		borderRadius: 8,
		borderWidth: 1,
		borderColor: Helper.opacityColor(Colors.PRIMARY_BLUE, 5),
	},
	form: {
		width: '90%',
		color: Colors.TEXT_3,
	},
	buttonFilter: {
		backgroundColor: Colors.WHITE,
		borderRadius: 8,
		alignItems: 'center',
		justifyContent: 'center',
		width: 43,
		height: 43,
		borderWidth: 1,
		borderColor: Helper.opacityColor(Colors.PRIMARY_BLUE, 5),
	},
	dropDownContainer: {
		width: 300,
		marginTop: -1,
		marginLeft: 15,
		borderRadius: 8,
		maxHeight: 150,
		elevation: 30,
		...Mixins.boxShadow('#eee'),
		backgroundColor: Colors.WHITE,
		borderWidth: 1,
		borderColor: Helper.opacityColor(Colors.PRIMARY_BLUE, 5),
		borderTopWidth: 0,
		borderTopLeftRadius: 0,
		borderTopRightRadius: 0,
		zIndex: 2,
	},
	dropDownItem: {
		paddingLeft: Mixins.scaleSize(15),
		paddingVertical: Mixins.scaleSize(10),
		borderBottomColor: '#EEEE',
		borderBottomWidth: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	backgroundClose: {
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width,
		position: 'absolute',
		zIndex: 1,
	},
	loaderHeader: {
		height: 32.5,
		width: 30,
		alignItems: 'center',
		alignContent: 'center',
		justifyContent: 'center',
	},
})