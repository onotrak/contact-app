import { Image, StyleSheet, View } from 'react-native'
import React from 'react'
import { Colors, Mixins } from '../../styles';
import { TextBold, TextRegular } from '../globals/Text';
import { Icon } from '../globals/Icon';
import { ImageAsset } from '../../utils';

const SplashComponent = ({version}) => {
  return (
      <View style={styles.container}>
         <Icon icon={ImageAsset.ContactLogo} size={200} />
         <TextBold text={'Contact App'} size={20} color={Colors.BLACK} />
         <View style={styles.viewVersion}>
            <TextRegular text={`Version ${version}`} />
         </View>
      </View>
   )
}

export default SplashComponent;

const styles = StyleSheet.create({
container: {
   flex: 1,
   backgroundColor: 'white',
   alignItems: 'center',
   justifyContent: 'center',
   paddingBottom: 40,
},
viewVersion: {
   position: 'absolute',
   bottom: 20,
},
});