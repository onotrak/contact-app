import { Dimensions, FlatList, Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Header } from '../globals/Header'
import { Colors, Mixins, Shadow } from '../../styles'
import { TextBold, TextMedium, TextRegular } from '../globals/Text'
import { Helper, ImageAsset } from '../../utils'
import { Touchable } from '../globals/Touchable'
import { useNavigation } from '@react-navigation/native'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Loader from '../globals/Loader'

const WIDTH = Dimensions.get('window').width;

const DetailComponent = ({dataDetail, onDeleteData, getDetail, data, loading}) => {
	const navigation = useNavigation();


   return (
      <View style={styles.container}>
         <Loader show={loading}/>
         <Header 
            iconBackColor={Colors.GREY_DARK} 
            style={{backgroundColor: Colors.WHITE}}
            title='Detail Contact' 
            rightComponent={
               <Touchable onPress={onDeleteData}>
                  <Entypo name='trash' size={20} color={Colors.RED} />
               </Touchable>
            }
         />
         <View style={styles.content}>
            <Image source={dataDetail?.photo && dataDetail.photo.includes('http') ? {uri: dataDetail?.photo} : ImageAsset.NotAvailable} style={styles.imgPhoto} />
            <View style={styles.card}>
               <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <TextMedium text={`${dataDetail?.firstName ?? '-'} ${dataDetail?.lastName ?? '-'}`} color={Colors.TEXT} size={20} style={{marginBottom: 8}} />
                  <Touchable onPress={()=> navigation.navigate('AddData', {data: dataDetail})}>
                     <FontAwesome name='edit' size={20} color={Colors.PRIMARY_BLUE} />
                  </Touchable>
               </View>
               <View style={styles.rowCard}>
                  <View style={styles.rowLeft}>
                     <TextRegular text={`First Name`} color={Colors.TEXT} />
                     <TextRegular text={`Last Name`} color={Colors.TEXT} />
                     <TextRegular text={`Age`} color={Colors.TEXT} />
                  </View>
                  <View style={styles.rowRight}>
                     <TextMedium text={`: ${dataDetail?.firstName ?? '-'}`} color={Colors.TEXT} />
                     <TextMedium text={`: ${dataDetail?.lastName ?? '-'}`} color={Colors.TEXT} />
                     <TextMedium text={`: ${dataDetail?.age ?? '-'}`} color={Colors.TEXT} />
                  </View>
               </View>               
            </View>
            <TextMedium text={`Contact`} color={Colors.TEXT} style={{marginTop: 40, marginBottom: 10}} size={17}/>
				<FlatList
					data={data}
               horizontal
					renderItem={({item}) => 
						<Item 
                     name={`${item.firstName} ${item.lastName}`} 
							firstName={`${item.firstName}`} 
							lastName={`${item.lastName}`} 
							photo={item.photo} 
							onPressDetail={()=> getDetail(item?.id??'')} 
                     isSelected={item?.id === dataDetail?.id}
						/>
					}
					keyExtractor={item => item?.id}
				/>
         </View>
      </View>
   )
}

const Item = ({name, firstName, lastName, photo, onPressDetail, isSelected}) => (
	<Touchable activeOpacity={0.5} onPress={onPressDetail} style={[styles.item, isSelected ? {borderColor: Colors.GREEN}:{}]}>
		{
			photo && photo.includes('http') ? 
			<Image source={{uri: photo}} style={styles.imgPhotoItem} />
			:
			<View style={styles.iconCard}>
				<TextBold text={Helper.iconItem(name)} size={20} color={Colors.PRIMARY_BLUE} />
			</View>
		}
		<TextRegular numberOfLines={1} text={firstName} style={styles.textName} color={Colors.TEXT}/>
	</Touchable>
);

export default DetailComponent

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: Colors.NEUTRAL,
   },
   content: {
      padding: 18,
		backgroundColor: Colors.NEUTRAL,
   },
   imgPhoto: {
      width: '100%',
      height: 200,
      resizeMode: 'contain',
   },
   card: {
      marginTop: 20,
      paddingHorizontal: 20,
      paddingVertical: 15,
      backgroundColor: Colors.WHITE,
      borderRadius: 8,
      ...Shadow.Normal,
   },
   buttonUpdate: {
      backgroundColor: Colors.YELLOW,
      paddingVertical: 10,
      width: WIDTH-60,
      borderRadius: 8,
      alignItems: 'center',
      alignSelf: 'center',
   },
   buttonDelete: {
      backgroundColor: Colors.RED,
      paddingVertical: 10,
      width: WIDTH-60,
      borderRadius: 8,
      alignItems: 'center',
      marginTop: 10,
      alignSelf: 'center',
   },
   buttonAdd: {
      backgroundColor: Colors.PRIMARY_BLUE,
      paddingVertical: 10,
      width: WIDTH-40,
      borderRadius: 8,
      alignItems: 'center',
   },
   viewButtonAdd: {
      position: 'absolute',
      bottom: 15,
      width: WIDTH,
      alignItems: 'center',
   },
   rowCard: {
      flexDirection: 'row',
   },
   rowLeft: {
      width: '35%',
   },
   rowRight: {
      width: '75%',
   },
	item: {
		marginRight: 10,
		marginHorizontal: 5,
		paddingHorizontal: 20,
		paddingVertical: 10,
		alignItems: 'center',
      backgroundColor: Colors.WHITE,
      borderRadius: 8,
		borderColor: Helper.opacityColor(Colors.PRIMARY_BLUE, 5),
		borderWidth: 1,
      marginBottom: 10,
      ...Shadow.Normal,
	},
	textName: {
		fontSize: 15,
      marginTop: 10,
      textAlign: 'center',
      width: 70,
	},
	imgPhotoItem: {
		width: 50,
		height: 50,
		borderRadius: 100,
		resizeMode: 'cover'
	},
	iconCard: {
		width: 48.5,
		height: 48.5,
		borderRadius: 48.5,
		alignItems: 'center',
		justifyContent: 'center',
		borderColor: Colors.PRIMARY_BLUE,
		borderWidth: 1.5,
	},
})