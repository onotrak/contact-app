import React from 'react';
import {View, StyleSheet} from 'react-native';
import { TextMedium } from './Text';
import { opacityColor } from '../../utils/helpers';
import { Colors, Mixins } from '../../styles';
import { IconAsset } from '../../utils';
import { Touchable } from './Touchable';
import { Icon } from './Icon';
import { useNavigation } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const Header = ({
	style,
	title = 'Header Title', 
	titleColor = opacityColor(Colors.BLACK, 7),
	iconBack,
	iconBackColor = Colors.PRIMARY,
	iconBackSize = 25,
	disabledBack,
	onBack,

	iconRightSize = 30,
	iconRight,
	onPressIconRight,
	iconRightColor,
	rightComponent,
	middleComponent,
}) => {
	const navigation = useNavigation();

	return (
		<View style={[styles.container, style]}>
			<Touchable 
				onPress={onBack ? onBack : () => navigation.goBack()} 
				disabled={disabledBack}
				style={styles.backButton}
			>
				{
					iconBack ?
					<Icon icon={iconBack} color={disabledBack ? 'transparent' : iconBackColor} size={iconBackSize} />
					:
					<Ionicons name='chevron-back' size={iconBackSize} color={iconBackColor} />
				}
			</Touchable>
			<View 
				style={[
					styles.titleContent,
					iconRight ? {paddingRight: Mixins.scaleSize(48)}:{},
				]}
			>
				{
					middleComponent ??
					<TextMedium 
						numberOfLines={1} 
						size={18} 
						text={title} 
						color={titleColor}
					/>
				}
			</View>
			<View style={styles.rightContent}>
				{
					iconRight &&
					<Touchable onPress={onPressIconRight} style={styles.iconRight} >
						<Icon icon={iconRight} color={iconRightColor} size={iconRightSize} />
					</Touchable>
				}
				{rightComponent}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		borderBottomColor: Colors.GREY_2,
		borderBottomWidth: 1,
	},
	backButton: {
		padding: Mixins.scaleSize(10),
	},
	titleContent: {
		width: '100%',
		alignItems: 'center',
		position: 'absolute',
		paddingHorizontal: Mixins.scaleSize(50),
		zIndex: -1,
	},
	rightContent: {
		flexDirection: 'row',
		paddingRight: Mixins.scaleSize(15),
	},
	iconRight: {
		paddingVertical: Mixins.scaleSize(10),
		paddingRight: Mixins.scaleSize(15),
	},
});
