import * as Header from './Header';
import * as Icon from './Icon';
import * as InputText from './InputText';
import * as Loader from './Loader';
import * as PlaceholderOpacity from './PlaceholderOpacity';
import * as PlaceholderSlide from './PlaceholderSlide';
import * as Text from './Text';
import * as Touchable from './Touchable';

export {Header, Icon, InputText, Loader, PlaceholderOpacity, PlaceholderSlide, Text, Touchable};