import React from 'react';
import {StyleSheet, View, TextInput, Platform} from 'react-native';
import { Colors, Mixins, Shadow, Typography } from '../../styles';
import { Touchable } from './Touchable';
import { Icon } from './Icon';
import { TextRegular } from './Text';
import { Helper } from '../../utils';

export const InputText = ({
	autoFocus,
	style,
	floating,
	floatingStyle = {},
	editable,
	secureTextEntry,
	onFocus,
	onBlur,
	blurOnSubmit,
	placeholder = 'Input Text',
	placeholderTextColor = Colors.GREY_1,
	keyboardType = 'default',
	autoCapitalize = 'none',
	value = '',
	onChangeText,
	icon,
	iconColor = Colors.GREY_1,
	iconSize = 20,
	onPressRightBtn,
	iconLeft,
	iconLeftColor = Colors.GREY_1,
	iconLeftSize = 20,
	onPressLeftBtn,
	message,
	messageColor = Colors.GREY_1,
	multiline = false,
	numberOfLines,
	height,
	valueColor,
	fontFamily = Typography.FONT_SEMI_BOLD,
	fontSize = 15,
	onSubmitEditing,
	testID
}) => {
	return (
		<View 
			style={[
				styles.container, 
				style, 
				floating && {paddingTop: Mixins.scaleSize(15)}, 
				message ? {marginBottom: Mixins.scaleSize(25)} : {marginBottom: 20},
			]}
		>
			{
				floating && value ? 
				<TextRegular 
					text={placeholder} 
					size={12} 
					color={placeholderTextColor} 
					style={[
						styles.floating,
						floatingStyle,
					]} 
				/>
				: null
			}
			{ 
				iconLeft && 
				<Touchable
					onPress={onPressLeftBtn}
					style={styles.leftButton}
				>
					<Icon icon={iconLeft} size={iconLeftSize} color={iconLeftColor} /> 
				</Touchable>
			}
			<TextInput
				editable={editable}
				autoFocus={autoFocus}
				secureTextEntry={secureTextEntry}
				onFocus={onFocus}
				onBlur={onBlur}
				placeholder={placeholder}
				placeholderTextColor={placeholderTextColor}
				keyboardType={keyboardType}
				autoCapitalize={autoCapitalize}
				value={value}
				onSubmitEditing={onSubmitEditing}
				multiline={multiline}
				numberOfLines={numberOfLines}
				blurOnSubmit={blurOnSubmit}
				onChangeText={onChangeText}
				accessible={true}
				accessibilityLabel={testID}
				testID={testID}
				style={[
					styles.content, 
					fontFamily, 
					{
						fontSize,
						width: icon && iconLeft ? '75%' : icon ? '85%': '100%', 
						height: !height && Platform.OS === 'ios' ? 40:height,
						marginTop: Platform.OS === 'ios' ? 10:2, 
						textAlignVertical: 'top',
						color: valueColor ? valueColor : Colors.BLACK,
					}
				]}
			/>
			{ 
				icon && 
				<Touchable
					onPress={onPressRightBtn}
					style={styles.rightButton}
				>
					<Icon icon={icon} size={iconSize} color={iconColor} /> 
				</Touchable>
			}
			{
				!message ? null: 
				<TextRegular text={message} size={12} color={messageColor} style={styles.message} />
			}
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		alignItems: 'flex-end',
		justifyContent: 'space-between',
		flexDirection: 'row',
	},
	content: {
		height: 45,
		fontSize: 14,
		paddingLeft: 0,
		paddingBottom: 5,
		paddingTop: Platform.OS === 'ios' ? 0 : 5,
	},
	rightButton: {
		width: 35, 
		alignItems: 'center',
		alignSelf: 'flex-end',
		paddingVertical: Mixins.scaleSize(5),
		backgroundColor: Helper.opacityColor(Colors.PRIMARY_BLUE, 3),
		borderRadius: 8,
	},
	leftButton: {
		width: 35, 
		alignItems: 'center',
		paddingVertical: Mixins.scaleSize(5),
		marginRight: Mixins.scaleSize(10),
	},
	floating: {
		position: 'absolute',
		top: 10,
	},
	message: {
		position: 'absolute',
		bottom: -17,
		right: 0,
	},
});
