import React from 'react';
import {TouchableOpacity, View} from 'react-native';

export const Touchable = ({children, onPress, disabled, style, activeOpacity=0.1, testID, onLongPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      onPress={onPress}
      onLongPress={onLongPress}
      disabled={disabled}
      accessible={true}
      accessibilityLabel={testID}
      testID={testID}
    >
      <View style={[style, disabled && {opacity: 0.5}]}>{children}</View>
    </TouchableOpacity>
  );
};
