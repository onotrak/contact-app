import React, { useState } from 'react';
import {
	View,
	Modal,
	SafeAreaView,
	StyleSheet,
	Dimensions,
	TouchableOpacity,
	FlatList
} from 'react-native';
import {Colors, Mixins} from '../../styles';
import { Helper } from '../../utils';
import { Touchable } from '../globals/Touchable';
import { TextBold, TextMedium } from '../globals/Text';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const ModalAddData = ({
	show,
	onClose,
	backgroundClose = false,
	onSort,
	checked,
	setChecked,
}) => {
	const data = [
		{
			val: 'A - Z',
			type: 'ASC',
		},
		{
			val: 'Z - A',
			type: 'DESC',
		},
	]

	return (
		<Modal 
			visible={show} 
			transparent={true}
			onDismiss={backgroundClose ? onClose : ()=> {}}
			onRequestClose={backgroundClose ? onClose : ()=> {}}
			animationType='fade'
		>
			<SafeAreaView>
				<View style={styles.container}>
					<TouchableOpacity disabled={!backgroundClose} activeOpacity={1} onPress={onClose} style={styles.backgroundClose}/>
					<View style={styles.content}>
						<View style={[styles.itemTop, {paddingRight: 0}]}>
							<TextBold text={`Sort List`} size={17} color={Colors.BLUE_NAVY} />
							<Touchable style={{paddingHorizontal: Mixins.scaleSize(16)}} onPress={onClose}>
								<Ionicons
									name="md-close"
									size={20}
									color={Colors.BLUE_NAVY}
								/>
							</Touchable>
						</View>
						<FlatList
							showsVerticalScrollIndicator={false}
							data={data}
							style={styles.flatList}
							renderItem={({item, index}) => (
								<Touchable 
									onPress={()=> setChecked(checked === item.type ? null:item.type)}
									style={styles.itemList}
								>
									<TextMedium text={item.val} size={17} color={Colors.BLUE_NAVY} />
									<MaterialCommunityIcons
										name={checked === item.type ? "radiobox-marked":"radiobox-blank"}
										size={20}
										color={Colors.PRIMARY_BLUE}
									/>
								</Touchable>
							)}
						/>
						<Touchable onPress={()=> onSort(checked)} style={styles.buttonAdd}>
							<TextBold text={'Save'} size={16} color={Colors.WHITE} />
						</Touchable>
					</View>
				</View>
			</SafeAreaView>
		</Modal>
	);
};

export default ModalAddData;

const styles = StyleSheet.create({
   container: {
		backgroundColor: Helper.opacityColor(Colors.BLACK, 5),
		justifyContent: 'flex-end',
		width: '100%',
		height: '100%',
   },
	backgroundClose: {
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width,
		position: 'absolute',
	},
	content: {
		backgroundColor: Colors.NEUTRAL,
		borderRadius: 5,
		width: '100%',
	},
	addButton: {
		borderRadius: 8, 
		borderColor: Colors.BLUE_NAVY,
		borderWidth: 1,
		paddingHorizontal: 20,
		paddingVertical: 10,
		alignItems: 'center',
	},
   buttonAdd: {
      backgroundColor: Colors.PRIMARY_BLUE,
      paddingVertical: 10,
		margin: 20,
      borderRadius: 8,
      alignItems: 'center',
   },
   itemTop: {
      padding: Mixins.scaleSize(16),
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderBottomColor: Colors.GREY_BORDER,
      borderBottomWidth: 1,
   },
   itemList: {
      paddingVertical: Mixins.scaleSize(18),
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderBottomColor: Colors.GREY_BORDER,
      borderBottomWidth: 1,
   },
   flatList: {
      paddingHorizontal: Mixins.scaleSize(16),
      marginBottom: Mixins.scaleSize(8),
   },
});