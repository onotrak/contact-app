import React from 'react';
import {
	View,
	Modal,
	SafeAreaView,
	StyleSheet,
	Dimensions,
	TouchableOpacity,
	Image,
	FlatList
} from 'react-native';
import {Colors, Mixins} from '../../styles';
import { Helper } from '../../utils';
import { Touchable } from '../globals/Touchable';
import { PRIMARY_BLUE, TEXT_3 } from '../../styles/colors';
import { TextMedium, TextRegular } from '../globals/Text';
import { useSelector } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';

const HEIGHT = Dimensions.get('window').height
const WIDTH = Dimensions.get('window').width

const ModalImage = ({
	show,
	onClose,
	backgroundClose = false,
	onSelect,
}) => {
	const { imageForm } = useSelector((state) => state.form);

	return (
		<Modal 
			visible={show} 
			transparent={true}
			onDismiss={backgroundClose ? onClose : ()=> {}}
			onRequestClose={backgroundClose ? onClose : ()=> {}}
			animationType='fade'
		>
			<SafeAreaView>
				<View style={styles.container}>
					<TouchableOpacity disabled={!backgroundClose} activeOpacity={1} onPress={onClose} style={styles.backgroundClose}/>
					<View style={styles.content}>
						<View style={styles.floatingBtn}>
							<Touchable activeOpacity={1} onPress={onClose}>
								<Ionicons name='close-circle' size={30} color={Colors.RED} />
							</Touchable>
						</View>
						<TextMedium text={'Select Image'} size={18} color={TEXT_3} style={styles.titleText} />
						<FlatList
							data={imageForm}
							numColumns={2}
							columnWrapperStyle={{flexWrap: 'wrap', justifyContent: 'center'}}
							renderItem={({item}) => 
								<Item 
									image={item} 
									onPress={onSelect} 
								/>
							}
						/>
					</View>
				</View>
			</SafeAreaView>
		</Modal>
	);
};

const Item = ({image, onPress}) => (
	<Touchable onPress={()=> onPress(image)} style={styles.item}>
		<Image source={{uri: image}} style={styles.imgPhoto} />
	</Touchable>
);

export default ModalImage;

const styles = StyleSheet.create({
   container: {
		backgroundColor: Helper.opacityColor(Colors.BLACK, 5),
		justifyContent: 'center',
		alignItems: 'center',
		padding: Mixins.scaleSize(20),
		width: '100%',
		height: '100%',
   },
	backgroundClose: {
		height: HEIGHT,
		width: WIDTH,
		position: 'absolute',
	},
	content: {
		backgroundColor: Colors.NEUTRAL,
		padding: Mixins.scaleSize(10),
		borderRadius: Mixins.scaleSize(10),
		borderTopRightRadius: 0,
		width: '95%',
		height: '95%',
	},
	titleText: {
		marginBottom: 10,
		borderBottomWidth: 1,
		borderBottomColor: PRIMARY_BLUE,
		paddingBottom: 5,
		textAlign: 'center',
	},
	item: {
		margin: 5,
		padding: 5, 
		width: WIDTH/2-55,
		borderRadius: 5,
		borderWidth: 1,
		borderColor: PRIMARY_BLUE,
	},
	imgPhoto: {
		width: '100%',
		height: 100,
		resizeMode: 'contain',
	},
	floatingBtn: {
		position: 'absolute',
		top: -10,
		right: -10,
	},
});