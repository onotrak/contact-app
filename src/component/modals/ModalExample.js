import React from 'react';
import {
	View,
	Modal,
	SafeAreaView,
	StyleSheet,
	Dimensions,
	TouchableOpacity
} from 'react-native';
import {Colors, Mixins} from '../../styles';
import { Helper } from '../../utils';

const ModalExample = ({
	show,
	onClose,
	backgroundClose = false,
}) => {
	return (
		<Modal 
			visible={show} 
			transparent={true}
			onDismiss={backgroundClose ? onClose : ()=> {}}
			onRequestClose={backgroundClose ? onClose : ()=> {}}
			animationType='fade'
		>
			<SafeAreaView>
				<View style={styles.container}>
					<TouchableOpacity disabled={!backgroundClose} activeOpacity={1} onPress={onClose} style={styles.backgroundClose}/>
					<View style={styles.content}>
					</View>
				</View>
			</SafeAreaView>
		</Modal>
	);
};

export default ModalExample;

const styles = StyleSheet.create({
   container: {
		backgroundColor: Helper.opacityColor(Colors.BLACK, 5),
		justifyContent: 'center',
		alignItems: 'center',
		padding: Mixins.scaleSize(20),
		width: '100%',
		height: '100%',
   },
	backgroundClose: {
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width,
		position: 'absolute',
	},
	content: {
		backgroundColor: Colors.NEUTRAL,
		padding: Mixins.scaleSize(20),
	},
});